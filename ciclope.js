 // El ciclope
 import Personaje from './personaje';
 export default class Ciclope extends Personaje {
    constructor(nombre, saludo, vida, fuerza){
        super(nombre, saludo, vida);
        this.fuerza = fuerza
    }

    ataque(){
        alert("El Ciclope te ha atacado con fuerza");
    }
 }  

//  //Como una mejora; intentare colocar un solo archivo donde instanciare a todas las clases
//  en un fiechero que voy a llamar play (Con el siguiente codigo FUNCIONA!!)


//  const ciclope = new Ciclope("Ciclope","El Ciclope imortal", 150,
//    "El ciclope te ha atacado con fuerza");

//  var ataque = document.querySelector("#b_ciclope");
//  ataque.addEventListener("click", ciclope.ataque);
// //  //console.log(ciclope.mostrarSaludo("Soy el Ciclope"));


 

 